2018-08-20 Moore v30r1
========================================

Checkpoint release from master
----------------------------------------
Based on Gaudi v30r3, LHCb v50r1, Lbcom v30r1, Rec v30r1, Phys v30r1, Hlt v30r1.
This version is released on the master branch.

- Remove L0App, !158 (@rmatev)
- Use upgrade minbias LDST for tests, !145 (@olupton)
- Merge 2018 into master, !142 (@rmatev) 
  Ports !157, !156, !155, !130, !152, !151, !148, !80, !144, !140, !139, !137, !136, !135, !129, !133, !131
