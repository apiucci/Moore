Developing Moore
================

Whether you want to edit the C++ algorithms used in Moore or Moore's Python
configuration, it's generally simplest to use the `lb-stack-setup`_ utility.

The README provided on the `project homepage <lb-stack-setup>`_ will guide you
through steps that will give you full check-outs of Moore and all of it's LHCb
and Gaudi dependencies (such as the Phys and Rec projects). You are then free
to edit any file in any project, re-build, and re-run Moore to see what
changes!

When editing files that `live in Moore <Moore>`_, there are a few conventions
to follow which are listed on this page, but before then it's important to
check the edits don't break the tests!


Running tests
-------------

A good way to start developing is to run the tests, so-called 'QM tests', which
are run automatically in the `nightly build system`_. This has the big advantage
that you start from an options file which is guaranteed to work and you can
test yourself that you are not introducing any unwanted changes.

You can find out which tests are available by running::

    make Moore/test ARGS="-N"

The specification of most tests is done in files with a ``.qmt`` extension,
which are located in directories like ``Hlt/Hlt2Conf/tests/qmtest``. The name
of a test is given by the directory it is in and the filename. Inside the
``.qmt`` file you will find the options which are executed by the test and what
the requirements for the test are to pass.

To run a test, the following options are available::

    make Moore/test ARGS='-N'
    # Run all tests
    make Moore/test  
    # Run all tests with 4 parallel jobs
    make Moore/test ARGS='-j 4'  
    # Run test(s) matching a regular expression
    make Moore/test ARGS='-R hlt1_example$'
    # Verbose output showing test (failure) details
    make Moore/test ARGS='-R hlt1_example -V'
    # Run test(s) N to M
    make Moore/test ARGS='-I N,M'
    
You can also use ``make fast/Moore/test …`` if you have only changed code in the
Moore project. This skips checking upstream projects for changes.

Individal tests can also be run by hand with ``gaudirun.py``::

    ./Moore/run gaudirun.py Moore/<Hat>/<Package>/tests/qmtest/<test name>.qmt

A more detailed description can be found on the `Gaudi testing infrastructure`_
TWiki page (which is outdated in some places).

Coding conventions
------------------

When writing Python in Moore, you should try to follow the `PEP8 guidelines`_ and other general good practices:

* Include comments and documentation when the intent of a line, function,
  class, or module is not obvious.
* Factor code into functions when there is repetition and/or for clarity.
* When modifying an existing file, following the conventions of the surrounding
  code.

For examples of how to do things, just have a look around what already exists
in Moore.

Docstrings
^^^^^^^^^^

Moore follows `Google's docstring conventions <gstrings>`_. There is a nice
example of usage in the `Sphinx documentation <gexample>`_ [#sphinx-what]_  and lots within Moore itself.

Standard Python conventions, e.g. PEP8. Small, readable functions. Google-style docstrings.

Continuous integration
----------------------

Whenever a new commit is made to the Moore repository, a `CI pipeline`_ runs
that performs some style and syntax checks:

* The LHCb copyright statement should be present at the top of every source file.
* The LHCb Python formatting rules must be adhered to.
* The Python code must have valid syntax and not raise any `flake8 error codes`_.

.. _lb-stack-setup: https://gitlab.cern.ch/rmatev/lb-stack-setup
.. _Moore: https://gitlab.cern.ch/lhcb/Moore
.. _Gaudi testing infrastructure: https://twiki.cern.ch/twiki/bin/view/Gaudi/GaudiTestingInfrastructure
.. _nightly build system: https://lhcb-nightlies.cern.ch/nightly/summary/
.. _PEP8 guidelines: https://www.python.org/dev/peps/pep-0008/
.. _gstrings: https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings
.. _gexample: https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html#example-google
.. _Sphinx: https://www.sphinx-doc.org/
.. _CI pipeline: https://docs.gitlab.com/ee/ci/
.. _flake8 error codes: http://flake8.pycqa.org/en/2.5.5/warnings.html

.. rubric:: Footnotes

.. [#sphinx-what] `Sphinx`_ is the program used to generate the documentation web site
    you're currrently reading!
