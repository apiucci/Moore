# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = .
BUILDDIR      = _build

GRAPH_SCRIPTS := $(wildcard scripts/*.py)
CONTROL_GRAPH_SOURCES := $(patsubst scripts/%.py, graphviz/%_control_flow.gv, $(GRAPH_SCRIPTS))
DATA_GRAPH_SOURCES := $(patsubst scripts/%.py, graphviz/%_data_flow.gv, $(GRAPH_SCRIPTS))

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
html pdf: Makefile graphs
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

linkcheck: Makefile graphs
	@cat _certificates/*.crt >> $(shell python -m certifi)  # install CERN CA certificates
	@$(SPHINXBUILD) -b linkcheck "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

clean: Makefile
	@rm -rf graphviz
	@$(SPHINXBUILD) -M clean "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

graphs: $(DATA_GRAPH_SOURCES) $(CONTROL_GRAPH_SOURCES)

# Generate graphs with names based on the options file that creates them
graphviz/%_data_flow.gv graphviz/%_control_flow.gv: scripts/%.py
	@mkdir -p graphviz
	@gaudirun.py -n $<
	@mv data_flow.gv graphviz/$*_data_flow.gv
	@mv control_flow.gv graphviz/$*_control_flow.gv

.PHONY: help html pdf clean graphs Makefile
