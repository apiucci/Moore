###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.BaseTest import LineSkipper, BlockSkipper
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor
preprocessor = LineSkipper(regexps=[
    # expected WARNINGs from the data broker
    r"HiveDataBrokerSvc +WARNING non-reentrant algorithm: .*",
    # expected WARNINGs from MuonIDHlt1Alg due to lhcb/Rec#79
    r"MuonIDHlt1Alg.* +WARNING CondDB {X,Y}FOIParameters member "
    r"size is 20, geometry expects 16",
    # expected WARNING when JIT-compiling functors
    r".*WARNING Suppressing message: 'Stack:AVX2, Functor:Scalar, instruction set mismatch. ROOT/cling was not compiled with the same options as the stack, try the functor cache'",
    # output from the scheduler that is expected to differ run-by-run
    r"HLTControlFlowMgr\s+INFO ---> End of Initialization. This took [0-9\.]+ ms",
    r"HLTControlFlowMgr\s+INFO ---> Loop over \d+ Events Finished -  WSS [0-9\.]+, timed \d+ Events: [0-9\.]+ ms, Evts/s = [0-9\.]+",
])

skipApplicationOptions = BlockSkipper("** User ApplicationOptions/",
                                      "End of User ApplicationOptions/")

ref_preprocessor = LHCbPreprocessor + preprocessor + skipApplicationOptions
