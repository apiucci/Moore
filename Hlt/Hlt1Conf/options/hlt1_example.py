###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from Hlt1Conf.settings import all_lines
from Hlt1Conf.lines.track_mva import debug_two_track_mva_line

options.set_input_from_testfiledb('MiniBrunel_2018_MinBias_FTv4_DIGI')
options.evt_max = 1000
options.scheduler_legacy_mode = False
options.set_conds_from_testfiledb('MiniBrunel_2018_MinBias_FTv4_DIGI')


def make_lines():
    return all_lines() + [
        debug_two_track_mva_line(),
    ]


# Uncomment the following to increase the output verbosity
# from Gaudi.Configuration import DEBUG
# options.output_level = DEBUG

# To adjust the data flow configuration, import a configurable
#   `from RecoConf.hlt1_tracking import default_ft_decoding_version`
# and wrap with
#   `with default_ft_decoding_version.bind(value=4):`

run_moore(options, make_lines)
