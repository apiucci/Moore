###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import mm
from PyConf import configurable
from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD_tracks
from ..algorithms import Filter

from Moore.config import HltLine

from Functors import CLOSESTTOBEAMZ, NHITS


@configurable
def smog_minimumbias_line(
        name='Hlt1_Smog_MinimumBias_Line',
        prescale=1,
        make_input_tracks=make_VeloClusterTrackingSIMD_tracks,
        max_Smog_z=-250 * mm,
        min_Smog_z=-550 * mm,
        min_nhits=17,
):
    selection = (CLOSESTTOBEAMZ < max_Smog_z) & (
        CLOSESTTOBEAMZ > min_Smog_z) & (NHITS > min_nhits)
    tracks = {'PrVeloTracks': make_input_tracks()['Pr']}
    track_filter = Filter(tracks, selection)['PrVeloTracks']

    return HltLine(name=name, algs=[track_filter], prescale=prescale)
