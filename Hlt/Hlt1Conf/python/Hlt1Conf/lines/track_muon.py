###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import math
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from Moore.config import HltLine
from RecoConf.hlt1_tracking import (
    require_gec,
    require_pvs,
    make_pvs,
    make_hlt1_tracks,
    make_hlt1_fitted_tracks,
)
from RecoConf.hlt1_muonid import (
    make_muon_id,
    make_tracks_with_muon_id,
)
from ..algorithms import Filter

from Functors import PT, CHI2DOF, MINIPCHI2, MINIPCHI2CUT, ISMUON
import Functors.math as fmath


def make_fitted_tracks_with_muon_id():
    all_tracks = make_hlt1_tracks()
    fitted_forward_tracks = make_hlt1_fitted_tracks(all_tracks)
    muon_ids = make_muon_id(all_tracks["Forward"])
    tracks_with_muon_id = make_tracks_with_muon_id(fitted_forward_tracks,
                                                   muon_ids)
    return {'PrFittedForwardWithMuonID': tracks_with_muon_id}


@configurable
def track_muon_prefilters(make_pvs=make_pvs):
    return [require_gec(), require_pvs(make_pvs())]


@configurable
def one_track_muon_mva_line(
        name='Hlt1TrackMuonMVALine',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        make_pvs=make_pvs,
        # TrackMuonLoose cuts from ZombieMoore to be done
        max_chi2dof=100.0,
        min_pt=2.0 * GeV,
        max_pt=26 * GeV,
        min_ipchi2=7.4,
        param1=1.0,
        param2=2.0,
        param3=1.248):
    pvs = make_pvs().location
    pre_sel = (ISMUON) & (PT > min_pt) & (CHI2DOF < max_chi2dof)
    hard_sel = (PT > max_pt) & MINIPCHI2CUT(IPChi2Cut=min_ipchi2, Vertices=pvs)
    bulk_sel = fmath.in_range(min_pt, PT, max_pt) & (
        fmath.log(MINIPCHI2(pvs)) >
        (param1 / ((PT / GeV - param2) * (PT / GeV - param2)) +
         (param3 / max_pt) * (max_pt - PT) + math.log(min_ipchi2)))
    full_sel = pre_sel & (hard_sel | bulk_sel)
    tracks_with_muon_id = make_input_tracks()
    track_filter = Filter(tracks_with_muon_id,
                          full_sel)['PrFittedForwardWithMuonID']
    return HltLine(
        name=name,
        algs=track_muon_prefilters() + [track_filter],
        prescale=prescale)
