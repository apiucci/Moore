###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from PyConf.application import configure_input, configure
from RecoConf.standalone import standalone_hlt1_reco
from RecoConf.hlt1_tracking import (
    all_hlt1_forward_track_types,
    make_PrForwardTracking_tracks,
)

config = configure_input(options)
with all_hlt1_forward_track_types.bind(
        make_forward_tracks=make_PrForwardTracking_tracks):
    top_cf_node = standalone_hlt1_reco()
config.update(configure(options, top_cf_node))
