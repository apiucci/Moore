###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.control_flow import CompositeNode, NodeLogic
from .hlt1_tracking import (
    require_gec, require_pvs, make_reco_pvs, make_pvs, make_hlt1_tracks,
    make_VeloKalman_fitted_tracks, all_velo_track_types,
    make_PrPixelTracking_tracks, make_PrForwardTracking_tracks,
    make_PatPV3DFuture_pvs, all_hlt1_forward_track_types,
    make_TrackEventFitter_fitted_tracks, make_hlt1_fitted_tracks)
from .hlt1_muonid import make_muon_id, make_tracks_with_muon_id
from .hlt2_tracking import make_hlt2_tracks
from .mc_checking import get_track_checkers, get_best_tracks_checkers


def reco_prefilters():
    return [require_gec()]


def standalone_hlt1_reco(do_mc_checking=False):
    hlt1_tracks = make_hlt1_tracks()
    fitted_tracks = make_VeloKalman_fitted_tracks(hlt1_tracks)
    muon_ids = make_muon_id(hlt1_tracks["Forward"])
    tracks_with_muon_id = make_tracks_with_muon_id(fitted_tracks, muon_ids)

    nodes = reco_prefilters() + [fitted_tracks["Pr"], tracks_with_muon_id]
    if do_mc_checking:
        nodes += [get_track_checkers(hlt1_tracks)]

    return CompositeNode(
        'hlt1_reco',
        children=nodes,
        combineLogic=NodeLogic.LAZY_AND,
        forceOrder=True)


def standalone_hlt2_full_track_reco(do_mc_checking=False):
    hlt2_tracks = make_hlt2_tracks()
    best_tracks = hlt2_tracks["Best"]
    pvs = make_pvs()
    nodes = reco_prefilters() + [best_tracks["v1"], pvs]
    if (do_mc_checking):
        types_and_locations_for_checkers = {
            "Velo": hlt2_tracks["Velo"],
            "VeloFull": hlt2_tracks["Velo"],
            "Upstream": hlt2_tracks["Upstream"],
            "Forward": hlt2_tracks["Forward"],
            "ForwardHlt1": hlt2_tracks["ForwardFastFitted"],
            "Seed": hlt2_tracks["Seed"],
            "Match": hlt2_tracks["Match"],
            "Downstream": hlt2_tracks["Downstream"],
        }
        nodes += [get_track_checkers(types_and_locations_for_checkers)]
        nodes += [get_best_tracks_checkers(best_tracks)]

    return CompositeNode(
        'lazy_and',
        children=nodes,
        combineLogic=NodeLogic.LAZY_AND,
        forceOrder=True)


def standalone_hlt2_full_track_reco_brunel(do_mc_checking=False):
    # we modify the velo tracks consistently for the pvs and the best tracks
    with all_velo_track_types.bind(make_velo_tracks=make_PrPixelTracking_tracks),\
         all_hlt1_forward_track_types.bind(make_forward_tracks=make_PrForwardTracking_tracks),\
         make_hlt1_fitted_tracks.bind(make_forward_fitted_tracks=make_TrackEventFitter_fitted_tracks),\
         make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs):
        return standalone_hlt2_full_track_reco(do_mc_checking)
